import { put, takeEvery, takeLatest, all, call } from 'redux-saga/effects'

export const delay = (ms) => new Promise(res => setTimeout(res, ms))


// Our worker Saga: will perform the async increment task
export function* incrementAsync() {
    yield call(delay, 1000)
    yield put({ type: 'INCREMENT' })
  }

// Our worker Saga: will perform the async increment task
export function* incrementLatestAsync() {
  yield call(delay, 2000)
  yield put({ type: 'INCREMENT' })
}
  
  // Our watcher Saga: spawn a new incrementAsync task on each INCREMENT_ASYNC
  export function* watchIncrementAsync() {
    yield takeEvery('INCREMENT_ASYNC', incrementAsync)
  }

  // Our watcher Saga: spawn a new incrementAsync task on each INCREMENT_ASYNC
  export function* watchIncrementLatestAsync() {
    yield takeLatest('INCREMENT_LATEST_ASYNC', incrementAsync)
  }

export function* helloSaga() {
    console.log('Hello Sagas!')
  }

  export default function* rootSaga() {
    yield all([
      helloSaga(),
      watchIncrementAsync(), 
      watchIncrementLatestAsync()
    ])
  }